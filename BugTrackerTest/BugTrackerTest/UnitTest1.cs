﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bug_Tracker.Classes;
using Bug_Tracker;
using Bug_Tracker.Forms.Classes;
using MySql.Data.MySqlClient;

namespace BugTrackerTest
{
    [TestClass]
    public class UnitTest1
    {
        //Test projectListManager creation
        [TestMethod]
        public void CreateProjectListManager()
        {
            Form1 bug = new Form1();
            ProjectListManager projectListManager = new ProjectListManager(bug);
        }

        //Test projectWindowManager creation
        [TestMethod]
        public void CreateWindowManager()
        {
            Form1 bug = new Form1();
            ProjectWindowManager projectWindowManager = new ProjectWindowManager(bug);
        }

        //Test database connection
        [TestMethod]
        public void MySQlExecTest()
        {
            mySQLExec mySQL = new mySQLExec();
            bool connected = false;
            MySqlDataReader reader = mySQL.Exec("SHOW TABLES LIKE \"reported_errors\"");
            while(reader.Read())
            {
                connected = true;
            }
            mySQL.Clean();

            Assert.AreEqual(true, connected);
        }

        //Test BitBucketRest class
        [TestMethod]
        public void BitBucketRESTTest()
        {
            BitBucketREST rest = new BitBucketREST();
            rest.RepoInfo("jamesajohnson", "advanced-software-a-bug-tracker");

            string expectedOwner = rest.owner_displayname;
            Assert.AreEqual("James Johnson", expectedOwner);
        }

        //test a string is encrypted and decrypted correctlye
        [TestMethod]
        public void CryptoTest()
        {
            Crypto crypto = new Crypto();
            string password = "password123";

            string hashed = crypto.EncryptStringAES(password, "secret");
            string unhashed = crypto.DecryptStringAES(hashed, "secret");

            Assert.AreEqual(password, unhashed);
        }
    }
}
