﻿using Bug_Tracker.Classes;
using Bug_Tracker.Forms;
using Bug_Tracker.Forms.Classes;
using FastColoredTextBoxNS;
using MetroFramework.Forms;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WeifenLuo.WinFormsUI.Docking;

namespace Bug_Tracker
{
    public partial class Form1 : MetroForm
    {
        //managers
        private ProjectListManager projectListManager;
        private ProjectWindowManager projectWindowManager;

        //connect to databse form
        ConnectToDatabase connectionForm = new ConnectToDatabase();

        public Form1()
        {
            InitializeComponent();
            
            projectListManager = new ProjectListManager(this);
            projectWindowManager = new ProjectWindowManager(this);
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            this.Show();
        }

        public ProjectWindowManager getProjectWindowManager {
            get
            {
                return projectWindowManager;
            }
            set
            {
                
            }
        }

        //database login cred
        public string serverAddress { get; internal set; }
        public string password { get; internal set; }
        public string userName { get; internal set; }
        public string databaseName { get; internal set; }

        //user login cred
        public string userID { get; internal set; } = "0";
        public string loginName { get; internal set; }
        public string loginPassword { get; internal set; }
    }
}
