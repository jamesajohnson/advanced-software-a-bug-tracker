﻿using MetroFramework.Controls;

namespace Bug_Tracker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.sptPageContainer = new System.Windows.Forms.SplitContainer();
            this.panel1 = new MetroFramework.Controls.MetroPanel();
            this.vScrollBar2 = new System.Windows.Forms.VScrollBar();
            this.projectListContainer = new MetroFramework.Controls.MetroPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.btnAddProject = new System.Windows.Forms.PictureBox();
            this.btnRemove = new System.Windows.Forms.PictureBox();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.tbcReport = new MetroFramework.Controls.MetroTabControl();
            this.tbHome = new System.Windows.Forms.TabPage();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.tbErrors = new MetroFramework.Controls.MetroTabPage();
            this.sptErrors = new System.Windows.Forms.SplitContainer();
            this.dtGridErrors = new MetroFramework.Controls.MetroGrid();
            this.tbcChatCode = new MetroFramework.Controls.MetroTabControl();
            this.tabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.messagesContainer = new MetroFramework.Controls.MetroPanel();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.panel3 = new MetroFramework.Controls.MetroPanel();
            this.txtComment = new MetroFramework.Controls.MetroTextBox();
            this.btnComment = new MetroFramework.Controls.MetroButton();
            this.tabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.txtCodeBox = new FastColoredTextBoxNS.FastColoredTextBox();
            this.tbReport = new MetroFramework.Controls.MetroTabPage();
            this.panel2 = new MetroFramework.Controls.MetroPanel();
            this.txtErrorDescription = new MetroFramework.Controls.MetroTextBox();
            this.btnSubmit = new MetroFramework.Controls.MetroButton();
            this.txtCode = new FastColoredTextBoxNS.FastColoredTextBox();
            this.label2 = new MetroFramework.Controls.MetroLabel();
            this.label1 = new MetroFramework.Controls.MetroLabel();
            this.dtGridFixes = new MetroFramework.Controls.MetroGrid();
            this.tbFixed = new System.Windows.Forms.TabPage();
            this.sptFixed = new System.Windows.Forms.SplitContainer();
            this.dtGridFixed = new MetroFramework.Controls.MetroGrid();
            this.tbBitBucket = new System.Windows.Forms.TabPage();
            this.metroPanel4 = new MetroFramework.Controls.MetroPanel();
            this.pnl_projectInfo = new MetroFramework.Controls.MetroPanel();
            this.lbl_projectDescription = new Bug_Tracker.UI_Components.GrowLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel9 = new MetroFramework.Controls.MetroPanel();
            this.pnl_RepoIssuesContainer = new MetroFramework.Controls.MetroPanel();
            this.pnl_RepoIssues = new MetroFramework.Controls.MetroPanel();
            this.scr_issues = new MetroFramework.Controls.MetroScrollBar();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel8 = new MetroFramework.Controls.MetroPanel();
            this.pb_avatar = new System.Windows.Forms.PictureBox();
            this.metroPanel5 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel7 = new MetroFramework.Controls.MetroPanel();
            this.btn_clnHTTPS = new MetroFramework.Controls.MetroLink();
            this.btn_clnSSH = new MetroFramework.Controls.MetroLink();
            this.lbl_bitName = new MetroFramework.Controls.MetroLabel();
            this.metroPanel6 = new MetroFramework.Controls.MetroPanel();
            this.lbl_lastUpdated = new MetroFramework.Controls.MetroLabel();
            this.lbl_projectOwner = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.sptPageContainer)).BeginInit();
            this.sptPageContainer.Panel1.SuspendLayout();
            this.sptPageContainer.Panel2.SuspendLayout();
            this.sptPageContainer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnAddProject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).BeginInit();
            this.metroPanel1.SuspendLayout();
            this.tbcReport.SuspendLayout();
            this.tbHome.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            this.tbErrors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sptErrors)).BeginInit();
            this.sptErrors.Panel1.SuspendLayout();
            this.sptErrors.Panel2.SuspendLayout();
            this.sptErrors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridErrors)).BeginInit();
            this.tbcChatCode.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeBox)).BeginInit();
            this.tbReport.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridFixes)).BeginInit();
            this.tbFixed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sptFixed)).BeginInit();
            this.sptFixed.Panel1.SuspendLayout();
            this.sptFixed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridFixed)).BeginInit();
            this.tbBitBucket.SuspendLayout();
            this.metroPanel4.SuspendLayout();
            this.pnl_projectInfo.SuspendLayout();
            this.metroPanel9.SuspendLayout();
            this.pnl_RepoIssuesContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_avatar)).BeginInit();
            this.metroPanel5.SuspendLayout();
            this.metroPanel7.SuspendLayout();
            this.metroPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // sptPageContainer
            // 
            this.sptPageContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sptPageContainer.Location = new System.Drawing.Point(20, 30);
            this.sptPageContainer.Name = "sptPageContainer";
            // 
            // sptPageContainer.Panel1
            // 
            this.sptPageContainer.Panel1.Controls.Add(this.panel1);
            // 
            // sptPageContainer.Panel2
            // 
            this.sptPageContainer.Panel2.Controls.Add(this.metroPanel1);
            this.sptPageContainer.Panel2.Controls.Add(this.metroLabel2);
            this.sptPageContainer.Size = new System.Drawing.Size(977, 442);
            this.sptPageContainer.SplitterDistance = 175;
            this.sptPageContainer.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.AllowDrop = true;
            this.panel1.Controls.Add(this.vScrollBar2);
            this.panel1.Controls.Add(this.projectListContainer);
            this.panel1.Controls.Add(this.metroLabel1);
            this.panel1.Controls.Add(this.metroPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.HorizontalScrollbarBarColor = true;
            this.panel1.HorizontalScrollbarHighlightOnWheel = false;
            this.panel1.HorizontalScrollbarSize = 10;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(175, 442);
            this.panel1.TabIndex = 5;
            this.panel1.VerticalScrollbarBarColor = true;
            this.panel1.VerticalScrollbarHighlightOnWheel = false;
            this.panel1.VerticalScrollbarSize = 10;
            // 
            // vScrollBar2
            // 
            this.vScrollBar2.Dock = System.Windows.Forms.DockStyle.Right;
            this.vScrollBar2.LargeChange = 1;
            this.vScrollBar2.Location = new System.Drawing.Point(158, 25);
            this.vScrollBar2.Maximum = 0;
            this.vScrollBar2.Name = "vScrollBar2";
            this.vScrollBar2.Size = new System.Drawing.Size(17, 389);
            this.vScrollBar2.TabIndex = 6;
            // 
            // projectListContainer
            // 
            this.projectListContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectListContainer.HorizontalScrollbarBarColor = true;
            this.projectListContainer.HorizontalScrollbarHighlightOnWheel = false;
            this.projectListContainer.HorizontalScrollbarSize = 10;
            this.projectListContainer.Location = new System.Drawing.Point(0, 25);
            this.projectListContainer.Name = "projectListContainer";
            this.projectListContainer.Size = new System.Drawing.Size(175, 389);
            this.projectListContainer.TabIndex = 5;
            this.projectListContainer.VerticalScrollbarBarColor = true;
            this.projectListContainer.VerticalScrollbarHighlightOnWheel = false;
            this.projectListContainer.VerticalScrollbarSize = 10;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(0, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(74, 25);
            this.metroLabel1.TabIndex = 4;
            this.metroLabel1.Text = "Projects";
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.btnAddProject);
            this.metroPanel2.Controls.Add(this.btnRemove);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(0, 414);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(175, 28);
            this.metroPanel2.TabIndex = 3;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // btnAddProject
            // 
            this.btnAddProject.Image = global::Bug_Tracker.Properties.Resources.plus;
            this.btnAddProject.Location = new System.Drawing.Point(120, 3);
            this.btnAddProject.Name = "btnAddProject";
            this.btnAddProject.Size = new System.Drawing.Size(23, 22);
            this.btnAddProject.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnAddProject.TabIndex = 3;
            this.btnAddProject.TabStop = false;
            // 
            // btnRemove
            // 
            this.btnRemove.Image = global::Bug_Tracker.Properties.Resources.delete;
            this.btnRemove.Location = new System.Drawing.Point(149, 3);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(23, 22);
            this.btnRemove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnRemove.TabIndex = 2;
            this.btnRemove.TabStop = false;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.tbcReport);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 25);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(798, 417);
            this.metroPanel1.TabIndex = 6;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // tbcReport
            // 
            this.tbcReport.Controls.Add(this.tbHome);
            this.tbcReport.Controls.Add(this.tbErrors);
            this.tbcReport.Controls.Add(this.tbReport);
            this.tbcReport.Controls.Add(this.tbFixed);
            this.tbcReport.Controls.Add(this.tbBitBucket);
            this.tbcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcReport.Location = new System.Drawing.Point(0, 0);
            this.tbcReport.Name = "tbcReport";
            this.tbcReport.SelectedIndex = 4;
            this.tbcReport.Size = new System.Drawing.Size(798, 417);
            this.tbcReport.TabIndex = 3;
            this.tbcReport.UseSelectable = true;
            // 
            // tbHome
            // 
            this.tbHome.Controls.Add(this.metroPanel3);
            this.tbHome.Location = new System.Drawing.Point(4, 38);
            this.tbHome.Name = "tbHome";
            this.tbHome.Size = new System.Drawing.Size(790, 375);
            this.tbHome.TabIndex = 2;
            this.tbHome.Text = "Home";
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.metroLabel4);
            this.metroPanel3.Controls.Add(this.metroLabel3);
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(0, 0);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(790, 375);
            this.metroPanel3.TabIndex = 0;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(31, 35);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(282, 19);
            this.metroLabel4.TabIndex = 3;
            this.metroLabel4.Text = "Please select a project from the left hand side. ";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.Location = new System.Drawing.Point(13, 10);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(85, 25);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Welcome";
            // 
            // tbErrors
            // 
            this.tbErrors.Controls.Add(this.sptErrors);
            this.tbErrors.HorizontalScrollbarBarColor = true;
            this.tbErrors.HorizontalScrollbarHighlightOnWheel = false;
            this.tbErrors.HorizontalScrollbarSize = 10;
            this.tbErrors.Location = new System.Drawing.Point(4, 38);
            this.tbErrors.Name = "tbErrors";
            this.tbErrors.Padding = new System.Windows.Forms.Padding(3);
            this.tbErrors.Size = new System.Drawing.Size(790, 375);
            this.tbErrors.TabIndex = 0;
            this.tbErrors.Text = "Current Bugs";
            this.tbErrors.UseVisualStyleBackColor = true;
            this.tbErrors.VerticalScrollbarBarColor = true;
            this.tbErrors.VerticalScrollbarHighlightOnWheel = false;
            this.tbErrors.VerticalScrollbarSize = 10;
            // 
            // sptErrors
            // 
            this.sptErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sptErrors.Location = new System.Drawing.Point(3, 3);
            this.sptErrors.Name = "sptErrors";
            this.sptErrors.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // sptErrors.Panel1
            // 
            this.sptErrors.Panel1.Controls.Add(this.dtGridErrors);
            // 
            // sptErrors.Panel2
            // 
            this.sptErrors.Panel2.Controls.Add(this.tbcChatCode);
            this.sptErrors.Size = new System.Drawing.Size(784, 369);
            this.sptErrors.SplitterDistance = 203;
            this.sptErrors.TabIndex = 1;
            // 
            // dtGridErrors
            // 
            this.dtGridErrors.AllowUserToResizeRows = false;
            this.dtGridErrors.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dtGridErrors.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtGridErrors.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dtGridErrors.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGridErrors.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dtGridErrors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtGridErrors.DefaultCellStyle = dataGridViewCellStyle2;
            this.dtGridErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtGridErrors.EnableHeadersVisualStyles = false;
            this.dtGridErrors.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dtGridErrors.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dtGridErrors.Location = new System.Drawing.Point(0, 0);
            this.dtGridErrors.MultiSelect = false;
            this.dtGridErrors.Name = "dtGridErrors";
            this.dtGridErrors.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGridErrors.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtGridErrors.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dtGridErrors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtGridErrors.Size = new System.Drawing.Size(784, 203);
            this.dtGridErrors.TabIndex = 0;
            // 
            // tbcChatCode
            // 
            this.tbcChatCode.Controls.Add(this.tabPage2);
            this.tbcChatCode.Controls.Add(this.tabPage3);
            this.tbcChatCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcChatCode.Location = new System.Drawing.Point(0, 0);
            this.tbcChatCode.Name = "tbcChatCode";
            this.tbcChatCode.SelectedIndex = 0;
            this.tbcChatCode.Size = new System.Drawing.Size(784, 162);
            this.tbcChatCode.TabIndex = 1;
            this.tbcChatCode.UseSelectable = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.messagesContainer);
            this.tabPage2.Controls.Add(this.vScrollBar1);
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.HorizontalScrollbarBarColor = true;
            this.tabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.tabPage2.HorizontalScrollbarSize = 10;
            this.tabPage2.Location = new System.Drawing.Point(4, 38);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(776, 120);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Chat";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.VerticalScrollbarBarColor = true;
            this.tabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.tabPage2.VerticalScrollbarSize = 10;
            // 
            // messagesContainer
            // 
            this.messagesContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messagesContainer.HorizontalScrollbarBarColor = true;
            this.messagesContainer.HorizontalScrollbarHighlightOnWheel = false;
            this.messagesContainer.HorizontalScrollbarSize = 10;
            this.messagesContainer.Location = new System.Drawing.Point(3, 3);
            this.messagesContainer.Margin = new System.Windows.Forms.Padding(3, 3, 20, 3);
            this.messagesContainer.Name = "messagesContainer";
            this.messagesContainer.Size = new System.Drawing.Size(753, 91);
            this.messagesContainer.TabIndex = 4;
            this.messagesContainer.VerticalScrollbarBarColor = true;
            this.messagesContainer.VerticalScrollbarHighlightOnWheel = false;
            this.messagesContainer.VerticalScrollbarSize = 10;
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.vScrollBar1.LargeChange = 1;
            this.vScrollBar1.Location = new System.Drawing.Point(756, 3);
            this.vScrollBar1.Maximum = 0;
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(17, 91);
            this.vScrollBar1.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtComment);
            this.panel3.Controls.Add(this.btnComment);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.HorizontalScrollbarBarColor = true;
            this.panel3.HorizontalScrollbarHighlightOnWheel = false;
            this.panel3.HorizontalScrollbarSize = 10;
            this.panel3.Location = new System.Drawing.Point(3, 94);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(770, 23);
            this.panel3.TabIndex = 3;
            this.panel3.VerticalScrollbarBarColor = true;
            this.panel3.VerticalScrollbarHighlightOnWheel = false;
            this.panel3.VerticalScrollbarSize = 10;
            // 
            // txtComment
            // 
            // 
            // 
            // 
            this.txtComment.CustomButton.Image = null;
            this.txtComment.CustomButton.Location = new System.Drawing.Point(673, 1);
            this.txtComment.CustomButton.Name = "";
            this.txtComment.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtComment.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtComment.CustomButton.TabIndex = 1;
            this.txtComment.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtComment.CustomButton.UseSelectable = true;
            this.txtComment.CustomButton.Visible = false;
            this.txtComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtComment.Lines = new string[0];
            this.txtComment.Location = new System.Drawing.Point(0, 0);
            this.txtComment.MaxLength = 32767;
            this.txtComment.Name = "txtComment";
            this.txtComment.PasswordChar = '\0';
            this.txtComment.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtComment.SelectedText = "";
            this.txtComment.SelectionLength = 0;
            this.txtComment.SelectionStart = 0;
            this.txtComment.Size = new System.Drawing.Size(695, 23);
            this.txtComment.TabIndex = 1;
            this.txtComment.UseSelectable = true;
            this.txtComment.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtComment.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnComment
            // 
            this.btnComment.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnComment.Location = new System.Drawing.Point(695, 0);
            this.btnComment.Name = "btnComment";
            this.btnComment.Size = new System.Drawing.Size(75, 23);
            this.btnComment.TabIndex = 0;
            this.btnComment.Text = "Comment";
            this.btnComment.UseSelectable = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtCodeBox);
            this.tabPage3.HorizontalScrollbarBarColor = true;
            this.tabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.tabPage3.HorizontalScrollbarSize = 10;
            this.tabPage3.Location = new System.Drawing.Point(4, 38);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(776, 120);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Code";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.VerticalScrollbarBarColor = true;
            this.tabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.tabPage3.VerticalScrollbarSize = 10;
            // 
            // txtCodeBox
            // 
            this.txtCodeBox.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.txtCodeBox.AutoScrollMinSize = new System.Drawing.Size(2, 14);
            this.txtCodeBox.BackBrush = null;
            this.txtCodeBox.CharHeight = 14;
            this.txtCodeBox.CharWidth = 8;
            this.txtCodeBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCodeBox.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodeBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeBox.IsReplaceMode = false;
            this.txtCodeBox.Location = new System.Drawing.Point(3, 3);
            this.txtCodeBox.Name = "txtCodeBox";
            this.txtCodeBox.Paddings = new System.Windows.Forms.Padding(0);
            this.txtCodeBox.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.txtCodeBox.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("txtCodeBox.ServiceColors")));
            this.txtCodeBox.Size = new System.Drawing.Size(770, 114);
            this.txtCodeBox.TabIndex = 1;
            this.txtCodeBox.Zoom = 100;
            // 
            // tbReport
            // 
            this.tbReport.Controls.Add(this.panel2);
            this.tbReport.Controls.Add(this.dtGridFixes);
            this.tbReport.HorizontalScrollbarBarColor = true;
            this.tbReport.HorizontalScrollbarHighlightOnWheel = false;
            this.tbReport.HorizontalScrollbarSize = 10;
            this.tbReport.Location = new System.Drawing.Point(4, 38);
            this.tbReport.Name = "tbReport";
            this.tbReport.Padding = new System.Windows.Forms.Padding(3);
            this.tbReport.Size = new System.Drawing.Size(790, 375);
            this.tbReport.TabIndex = 1;
            this.tbReport.Text = "Report a Bug";
            this.tbReport.UseVisualStyleBackColor = true;
            this.tbReport.VerticalScrollbarBarColor = true;
            this.tbReport.VerticalScrollbarHighlightOnWheel = false;
            this.tbReport.VerticalScrollbarSize = 10;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtErrorDescription);
            this.panel2.Controls.Add(this.btnSubmit);
            this.panel2.Controls.Add(this.txtCode);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.HorizontalScrollbarBarColor = true;
            this.panel2.HorizontalScrollbarHighlightOnWheel = false;
            this.panel2.HorizontalScrollbarSize = 10;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(784, 369);
            this.panel2.TabIndex = 1;
            this.panel2.VerticalScrollbarBarColor = true;
            this.panel2.VerticalScrollbarHighlightOnWheel = false;
            this.panel2.VerticalScrollbarSize = 10;
            // 
            // txtErrorDescription
            // 
            // 
            // 
            // 
            this.txtErrorDescription.CustomButton.Image = null;
            this.txtErrorDescription.CustomButton.Location = new System.Drawing.Point(697, 2);
            this.txtErrorDescription.CustomButton.Name = "";
            this.txtErrorDescription.CustomButton.Size = new System.Drawing.Size(59, 59);
            this.txtErrorDescription.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtErrorDescription.CustomButton.TabIndex = 1;
            this.txtErrorDescription.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtErrorDescription.CustomButton.UseSelectable = true;
            this.txtErrorDescription.CustomButton.Visible = false;
            this.txtErrorDescription.Lines = new string[0];
            this.txtErrorDescription.Location = new System.Drawing.Point(22, 27);
            this.txtErrorDescription.MaxLength = 32767;
            this.txtErrorDescription.Multiline = true;
            this.txtErrorDescription.Name = "txtErrorDescription";
            this.txtErrorDescription.PasswordChar = '\0';
            this.txtErrorDescription.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtErrorDescription.SelectedText = "";
            this.txtErrorDescription.SelectionLength = 0;
            this.txtErrorDescription.SelectionStart = 0;
            this.txtErrorDescription.Size = new System.Drawing.Size(759, 64);
            this.txtErrorDescription.TabIndex = 11;
            this.txtErrorDescription.UseSelectable = true;
            this.txtErrorDescription.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtErrorDescription.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(706, 296);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 10;
            this.btnSubmit.Text = "Submit ";
            this.btnSubmit.UseSelectable = true;
            // 
            // txtCode
            // 
            this.txtCode.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.txtCode.AutoIndentCharsPatterns = "\r\n^\\s*[\\w\\.]+(\\s\\w+)?\\s*(?<range>=)\\s*(?<range>[^;]+);\r\n^\\s*(case|default)\\s*[^:]" +
    "*(?<range>:)\\s*(?<range>[^;]+);\r\n";
            this.txtCode.AutoScrollMinSize = new System.Drawing.Size(2, 14);
            this.txtCode.BackBrush = null;
            this.txtCode.BracketsHighlightStrategy = FastColoredTextBoxNS.BracketsHighlightStrategy.Strategy2;
            this.txtCode.CharHeight = 14;
            this.txtCode.CharWidth = 8;
            this.txtCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCode.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCode.IsReplaceMode = false;
            this.txtCode.Language = FastColoredTextBoxNS.Language.CSharp;
            this.txtCode.LeftBracket = '(';
            this.txtCode.LeftBracket2 = '{';
            this.txtCode.Location = new System.Drawing.Point(22, 116);
            this.txtCode.Name = "txtCode";
            this.txtCode.Paddings = new System.Windows.Forms.Padding(0);
            this.txtCode.RightBracket = ')';
            this.txtCode.RightBracket2 = '}';
            this.txtCode.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.txtCode.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("txtCode.ServiceColors")));
            this.txtCode.Size = new System.Drawing.Size(759, 174);
            this.txtCode.TabIndex = 9;
            this.txtCode.Zoom = 100;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 19);
            this.label2.TabIndex = 8;
            this.label2.Text = "Relevant Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "Error Description";
            // 
            // dtGridFixes
            // 
            this.dtGridFixes.AllowUserToResizeRows = false;
            this.dtGridFixes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dtGridFixes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtGridFixes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dtGridFixes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGridFixes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dtGridFixes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtGridFixes.DefaultCellStyle = dataGridViewCellStyle5;
            this.dtGridFixes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtGridFixes.EnableHeadersVisualStyles = false;
            this.dtGridFixes.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dtGridFixes.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dtGridFixes.Location = new System.Drawing.Point(3, 3);
            this.dtGridFixes.Name = "dtGridFixes";
            this.dtGridFixes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGridFixes.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dtGridFixes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dtGridFixes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtGridFixes.Size = new System.Drawing.Size(784, 369);
            this.dtGridFixes.TabIndex = 0;
            // 
            // tbFixed
            // 
            this.tbFixed.Controls.Add(this.sptFixed);
            this.tbFixed.Location = new System.Drawing.Point(4, 38);
            this.tbFixed.Name = "tbFixed";
            this.tbFixed.Size = new System.Drawing.Size(790, 375);
            this.tbFixed.TabIndex = 3;
            this.tbFixed.Text = "Fixed Bugs";
            // 
            // sptFixed
            // 
            this.sptFixed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sptFixed.Location = new System.Drawing.Point(0, 0);
            this.sptFixed.Name = "sptFixed";
            this.sptFixed.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // sptFixed.Panel1
            // 
            this.sptFixed.Panel1.Controls.Add(this.dtGridFixed);
            this.sptFixed.Size = new System.Drawing.Size(790, 375);
            this.sptFixed.SplitterDistance = 198;
            this.sptFixed.TabIndex = 2;
            // 
            // dtGridFixed
            // 
            this.dtGridFixed.AllowUserToResizeRows = false;
            this.dtGridFixed.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dtGridFixed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtGridFixed.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dtGridFixed.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGridFixed.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dtGridFixed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtGridFixed.DefaultCellStyle = dataGridViewCellStyle8;
            this.dtGridFixed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtGridFixed.EnableHeadersVisualStyles = false;
            this.dtGridFixed.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dtGridFixed.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dtGridFixed.Location = new System.Drawing.Point(0, 0);
            this.dtGridFixed.MultiSelect = false;
            this.dtGridFixed.Name = "dtGridFixed";
            this.dtGridFixed.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtGridFixed.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dtGridFixed.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dtGridFixed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtGridFixed.Size = new System.Drawing.Size(790, 198);
            this.dtGridFixed.TabIndex = 0;
            // 
            // tbBitBucket
            // 
            this.tbBitBucket.Controls.Add(this.metroPanel4);
            this.tbBitBucket.Location = new System.Drawing.Point(4, 38);
            this.tbBitBucket.Name = "tbBitBucket";
            this.tbBitBucket.Size = new System.Drawing.Size(790, 375);
            this.tbBitBucket.TabIndex = 4;
            this.tbBitBucket.Text = "BitBucket";
            // 
            // metroPanel4
            // 
            this.metroPanel4.Controls.Add(this.pnl_projectInfo);
            this.metroPanel4.Controls.Add(this.metroPanel9);
            this.metroPanel4.Controls.Add(this.metroPanel8);
            this.metroPanel4.Controls.Add(this.pb_avatar);
            this.metroPanel4.Controls.Add(this.metroPanel5);
            this.metroPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel4.HorizontalScrollbarBarColor = true;
            this.metroPanel4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel4.HorizontalScrollbarSize = 10;
            this.metroPanel4.Location = new System.Drawing.Point(0, 0);
            this.metroPanel4.Name = "metroPanel4";
            this.metroPanel4.Size = new System.Drawing.Size(790, 375);
            this.metroPanel4.TabIndex = 2;
            this.metroPanel4.VerticalScrollbarBarColor = true;
            this.metroPanel4.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel4.VerticalScrollbarSize = 10;
            // 
            // pnl_projectInfo
            // 
            this.pnl_projectInfo.Controls.Add(this.lbl_projectDescription);
            this.pnl_projectInfo.Controls.Add(this.metroLabel6);
            this.pnl_projectInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_projectInfo.HorizontalScrollbarBarColor = true;
            this.pnl_projectInfo.HorizontalScrollbarHighlightOnWheel = false;
            this.pnl_projectInfo.HorizontalScrollbarSize = 10;
            this.pnl_projectInfo.Location = new System.Drawing.Point(200, 53);
            this.pnl_projectInfo.Name = "pnl_projectInfo";
            this.pnl_projectInfo.Size = new System.Drawing.Size(590, 298);
            this.pnl_projectInfo.TabIndex = 8;
            this.pnl_projectInfo.VerticalScrollbarBarColor = true;
            this.pnl_projectInfo.VerticalScrollbarHighlightOnWheel = false;
            this.pnl_projectInfo.VerticalScrollbarSize = 10;
            // 
            // lbl_projectDescription
            // 
            this.lbl_projectDescription.BackColor = System.Drawing.Color.White;
            this.lbl_projectDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_projectDescription.Location = new System.Drawing.Point(6, 25);
            this.lbl_projectDescription.Name = "lbl_projectDescription";
            this.lbl_projectDescription.Size = new System.Drawing.Size(100, 17);
            this.lbl_projectDescription.TabIndex = 11;
            this.lbl_projectDescription.Text = "growLabel1";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel6.Location = new System.Drawing.Point(0, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(165, 25);
            this.metroLabel6.TabIndex = 10;
            this.metroLabel6.Text = "Project Information";
            // 
            // metroPanel9
            // 
            this.metroPanel9.Controls.Add(this.pnl_RepoIssuesContainer);
            this.metroPanel9.Controls.Add(this.metroLabel5);
            this.metroPanel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.metroPanel9.HorizontalScrollbarBarColor = true;
            this.metroPanel9.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel9.HorizontalScrollbarSize = 10;
            this.metroPanel9.Location = new System.Drawing.Point(0, 53);
            this.metroPanel9.Name = "metroPanel9";
            this.metroPanel9.Size = new System.Drawing.Size(200, 298);
            this.metroPanel9.TabIndex = 5;
            this.metroPanel9.VerticalScrollbarBarColor = true;
            this.metroPanel9.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel9.VerticalScrollbarSize = 10;
            // 
            // pnl_RepoIssuesContainer
            // 
            this.pnl_RepoIssuesContainer.Controls.Add(this.pnl_RepoIssues);
            this.pnl_RepoIssuesContainer.Controls.Add(this.scr_issues);
            this.pnl_RepoIssuesContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_RepoIssuesContainer.HorizontalScrollbarBarColor = true;
            this.pnl_RepoIssuesContainer.HorizontalScrollbarHighlightOnWheel = false;
            this.pnl_RepoIssuesContainer.HorizontalScrollbarSize = 10;
            this.pnl_RepoIssuesContainer.Location = new System.Drawing.Point(0, 25);
            this.pnl_RepoIssuesContainer.Name = "pnl_RepoIssuesContainer";
            this.pnl_RepoIssuesContainer.Size = new System.Drawing.Size(200, 273);
            this.pnl_RepoIssuesContainer.TabIndex = 7;
            this.pnl_RepoIssuesContainer.VerticalScrollbarBarColor = true;
            this.pnl_RepoIssuesContainer.VerticalScrollbarHighlightOnWheel = false;
            this.pnl_RepoIssuesContainer.VerticalScrollbarSize = 10;
            // 
            // pnl_RepoIssues
            // 
            this.pnl_RepoIssues.HorizontalScrollbarBarColor = true;
            this.pnl_RepoIssues.HorizontalScrollbarHighlightOnWheel = false;
            this.pnl_RepoIssues.HorizontalScrollbarSize = 10;
            this.pnl_RepoIssues.Location = new System.Drawing.Point(0, 0);
            this.pnl_RepoIssues.Name = "pnl_RepoIssues";
            this.pnl_RepoIssues.Size = new System.Drawing.Size(184, 100);
            this.pnl_RepoIssues.TabIndex = 6;
            this.pnl_RepoIssues.VerticalScrollbarBarColor = true;
            this.pnl_RepoIssues.VerticalScrollbarHighlightOnWheel = false;
            this.pnl_RepoIssues.VerticalScrollbarSize = 10;
            // 
            // scr_issues
            // 
            this.scr_issues.Dock = System.Windows.Forms.DockStyle.Right;
            this.scr_issues.LargeChange = 10;
            this.scr_issues.Location = new System.Drawing.Point(190, 0);
            this.scr_issues.Maximum = 100;
            this.scr_issues.Minimum = 0;
            this.scr_issues.MouseWheelBarPartitions = 10;
            this.scr_issues.Name = "scr_issues";
            this.scr_issues.Orientation = MetroFramework.Controls.MetroScrollOrientation.Vertical;
            this.scr_issues.ScrollbarSize = 10;
            this.scr_issues.Size = new System.Drawing.Size(10, 273);
            this.scr_issues.TabIndex = 5;
            this.scr_issues.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel5.Location = new System.Drawing.Point(0, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(60, 25);
            this.metroLabel5.TabIndex = 6;
            this.metroLabel5.Text = "Issues";
            // 
            // metroPanel8
            // 
            this.metroPanel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel8.HorizontalScrollbarBarColor = true;
            this.metroPanel8.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel8.HorizontalScrollbarSize = 10;
            this.metroPanel8.Location = new System.Drawing.Point(0, 351);
            this.metroPanel8.Name = "metroPanel8";
            this.metroPanel8.Size = new System.Drawing.Size(790, 24);
            this.metroPanel8.TabIndex = 4;
            this.metroPanel8.VerticalScrollbarBarColor = true;
            this.metroPanel8.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel8.VerticalScrollbarSize = 10;
            // 
            // pb_avatar
            // 
            this.pb_avatar.Image = global::Bug_Tracker.Properties.Resources.bit_bucket;
            this.pb_avatar.Location = new System.Drawing.Point(0, 0);
            this.pb_avatar.Name = "pb_avatar";
            this.pb_avatar.Size = new System.Drawing.Size(50, 50);
            this.pb_avatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_avatar.TabIndex = 3;
            this.pb_avatar.TabStop = false;
            // 
            // metroPanel5
            // 
            this.metroPanel5.Controls.Add(this.metroPanel7);
            this.metroPanel5.Controls.Add(this.metroPanel6);
            this.metroPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel5.HorizontalScrollbarBarColor = true;
            this.metroPanel5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel5.HorizontalScrollbarSize = 10;
            this.metroPanel5.Location = new System.Drawing.Point(0, 0);
            this.metroPanel5.Name = "metroPanel5";
            this.metroPanel5.Size = new System.Drawing.Size(790, 53);
            this.metroPanel5.TabIndex = 2;
            this.metroPanel5.VerticalScrollbarBarColor = true;
            this.metroPanel5.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel5.VerticalScrollbarSize = 10;
            // 
            // metroPanel7
            // 
            this.metroPanel7.Controls.Add(this.btn_clnHTTPS);
            this.metroPanel7.Controls.Add(this.btn_clnSSH);
            this.metroPanel7.Controls.Add(this.lbl_bitName);
            this.metroPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel7.HorizontalScrollbarBarColor = true;
            this.metroPanel7.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel7.HorizontalScrollbarSize = 10;
            this.metroPanel7.Location = new System.Drawing.Point(0, 0);
            this.metroPanel7.Name = "metroPanel7";
            this.metroPanel7.Size = new System.Drawing.Size(590, 53);
            this.metroPanel7.TabIndex = 11;
            this.metroPanel7.VerticalScrollbarBarColor = true;
            this.metroPanel7.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel7.VerticalScrollbarSize = 10;
            // 
            // btn_clnHTTPS
            // 
            this.btn_clnHTTPS.Location = new System.Drawing.Point(128, 30);
            this.btn_clnHTTPS.Name = "btn_clnHTTPS";
            this.btn_clnHTTPS.Size = new System.Drawing.Size(83, 23);
            this.btn_clnHTTPS.TabIndex = 12;
            this.btn_clnHTTPS.Text = "Clone HTTPS";
            this.btn_clnHTTPS.UseSelectable = true;
            // 
            // btn_clnSSH
            // 
            this.btn_clnSSH.Location = new System.Drawing.Point(56, 30);
            this.btn_clnSSH.Name = "btn_clnSSH";
            this.btn_clnSSH.Size = new System.Drawing.Size(75, 23);
            this.btn_clnSSH.TabIndex = 11;
            this.btn_clnSSH.Text = "Clone SSH";
            this.btn_clnSSH.UseSelectable = true;
            // 
            // lbl_bitName
            // 
            this.lbl_bitName.AutoSize = true;
            this.lbl_bitName.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_bitName.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lbl_bitName.Location = new System.Drawing.Point(56, 2);
            this.lbl_bitName.Name = "lbl_bitName";
            this.lbl_bitName.Size = new System.Drawing.Size(106, 25);
            this.lbl_bitName.TabIndex = 10;
            this.lbl_bitName.Text = "lbl_bitName";
            // 
            // metroPanel6
            // 
            this.metroPanel6.Controls.Add(this.lbl_lastUpdated);
            this.metroPanel6.Controls.Add(this.lbl_projectOwner);
            this.metroPanel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.metroPanel6.HorizontalScrollbarBarColor = true;
            this.metroPanel6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel6.HorizontalScrollbarSize = 10;
            this.metroPanel6.Location = new System.Drawing.Point(590, 0);
            this.metroPanel6.Name = "metroPanel6";
            this.metroPanel6.Size = new System.Drawing.Size(200, 53);
            this.metroPanel6.TabIndex = 10;
            this.metroPanel6.VerticalScrollbarBarColor = true;
            this.metroPanel6.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel6.VerticalScrollbarSize = 10;
            // 
            // lbl_lastUpdated
            // 
            this.lbl_lastUpdated.AutoSize = true;
            this.lbl_lastUpdated.Location = new System.Drawing.Point(98, 25);
            this.lbl_lastUpdated.Name = "lbl_lastUpdated";
            this.lbl_lastUpdated.Size = new System.Drawing.Size(99, 19);
            this.lbl_lastUpdated.TabIndex = 15;
            this.lbl_lastUpdated.Text = "lbl_lastUpdated";
            // 
            // lbl_projectOwner
            // 
            this.lbl_projectOwner.AutoSize = true;
            this.lbl_projectOwner.Location = new System.Drawing.Point(88, 6);
            this.lbl_projectOwner.Name = "lbl_projectOwner";
            this.lbl_projectOwner.Size = new System.Drawing.Size(109, 19);
            this.lbl_projectOwner.TabIndex = 14;
            this.lbl_projectOwner.Text = "lbl_projectOwner";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel2.Location = new System.Drawing.Point(0, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(65, 25);
            this.metroLabel2.TabIndex = 5;
            this.metroLabel2.Text = "Report";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 492);
            this.Controls.Add(this.sptPageContainer);
            this.DisplayHeader = false;
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Text = "Bug Tracker";
            this.TransparencyKey = System.Drawing.Color.Empty;
            this.sptPageContainer.Panel1.ResumeLayout(false);
            this.sptPageContainer.Panel2.ResumeLayout(false);
            this.sptPageContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sptPageContainer)).EndInit();
            this.sptPageContainer.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnAddProject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).EndInit();
            this.metroPanel1.ResumeLayout(false);
            this.tbcReport.ResumeLayout(false);
            this.tbHome.ResumeLayout(false);
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            this.tbErrors.ResumeLayout(false);
            this.sptErrors.Panel1.ResumeLayout(false);
            this.sptErrors.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sptErrors)).EndInit();
            this.sptErrors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtGridErrors)).EndInit();
            this.tbcChatCode.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeBox)).EndInit();
            this.tbReport.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridFixes)).EndInit();
            this.tbFixed.ResumeLayout(false);
            this.sptFixed.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sptFixed)).EndInit();
            this.sptFixed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtGridFixed)).EndInit();
            this.tbBitBucket.ResumeLayout(false);
            this.metroPanel4.ResumeLayout(false);
            this.pnl_projectInfo.ResumeLayout(false);
            this.pnl_projectInfo.PerformLayout();
            this.metroPanel9.ResumeLayout(false);
            this.metroPanel9.PerformLayout();
            this.pnl_RepoIssuesContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_avatar)).EndInit();
            this.metroPanel5.ResumeLayout(false);
            this.metroPanel7.ResumeLayout(false);
            this.metroPanel7.PerformLayout();
            this.metroPanel6.ResumeLayout(false);
            this.metroPanel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private MetroPanel panel1;
        private MetroTabPage tabPage2;
        private MetroPanel panel3;
        private MetroTabPage tabPage3;
        private MetroLabel label2;
        private MetroLabel label1;
        private MetroGrid dtGridFixes;
        public MetroButton btnSubmit;
        public FastColoredTextBoxNS.FastColoredTextBox txtCode;
        public MetroGrid dtGridErrors;
        public MetroPanel messagesContainer;
        public System.Windows.Forms.VScrollBar vScrollBar1;
        public MetroTextBox txtComment;
        public MetroButton btnComment;
        public MetroTabControl tbcReport;
        public FastColoredTextBoxNS.FastColoredTextBox txtCodeBox;
        private MetroPanel panel2;
        public MetroTextBox txtErrorDescription;
        private MetroPanel metroPanel2;
        public System.Windows.Forms.PictureBox btnRemove;
        public System.Windows.Forms.PictureBox btnAddProject;
        private MetroLabel metroLabel1;
        public MetroPanel projectListContainer;
        private System.Windows.Forms.VScrollBar vScrollBar2;
        private MetroPanel metroPanel1;
        private MetroLabel metroLabel2;
        private MetroPanel metroPanel3;
        private MetroLabel metroLabel4;
        private MetroLabel metroLabel3;
        public System.Windows.Forms.TabPage tbHome;
        public MetroTabPage tbErrors;
        public MetroTabPage tbReport;
        public MetroGrid dtGridFixed;
        public System.Windows.Forms.TabPage tbFixed;
        public MetroTabControl tbcChatCode;
        public System.Windows.Forms.SplitContainer sptFixed;
        public System.Windows.Forms.SplitContainer sptPageContainer;
        public System.Windows.Forms.SplitContainer sptErrors;
        public System.Windows.Forms.TabPage tbBitBucket;
        private MetroPanel metroPanel4;
        private MetroPanel metroPanel5;
        private MetroPanel metroPanel6;
        public MetroLabel lbl_lastUpdated;
        public MetroLabel lbl_projectOwner;
        private MetroPanel metroPanel7;
        public MetroLabel lbl_bitName;
        public MetroLink btn_clnHTTPS;
        public MetroLink btn_clnSSH;
        public System.Windows.Forms.PictureBox pb_avatar;
        private MetroPanel metroPanel9;
        private MetroPanel metroPanel8;
        private MetroLabel metroLabel5;
        public MetroPanel pnl_RepoIssuesContainer;
        public MetroScrollBar scr_issues;
        public MetroPanel pnl_RepoIssues;
        public MetroPanel pnl_projectInfo;
        private MetroLabel metroLabel6;
        public UI_Components.GrowLabel lbl_projectDescription;
    }
}

