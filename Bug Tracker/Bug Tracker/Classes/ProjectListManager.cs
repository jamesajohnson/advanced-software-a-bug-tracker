﻿using Bug_Tracker.Forms;
using Bug_Tracker.UI_Components;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Windows.Forms;

namespace Bug_Tracker.Classes
{
    public class ProjectListManager
    {
        //connect to databse form
        ConnectToDatabase connectionForm = new ConnectToDatabase();

        //main project form
        private Form1 form1;

        //current project id
        private String currentProjectID;

        //sql manager
        private mySQLExec sql;

        public ProjectListManager(Form1 form1)
        {
            this.form1 = form1;

            sql = new mySQLExec();

            form1.btnAddProject.Click += this.btnAddProject_Click;
            form1.btnRemove.Click += this.btnRemove_Click;

            ListProjects();
        }

        private void ListProjects()
        {
            //clear list
            form1.projectListContainer.Controls.Clear();

            //read project list
            try
            {
                MySqlDataReader reader = sql.Exec("SELECT * FROM Projects");
                while (reader.Read())
                {
                    Project p = new Project();
                    p.projectName = reader["projectName"].ToString();
                    p.projectID = reader["ProjectID"].ToString();
                    p.projectDescription = reader["projectDescription"].ToString();
                    p.projectRepo = reader["projectRepo"].ToString();

                    myMetroLink projectLink = new myMetroLink(p);

                    projectLink.Click += delegate
                    {
                        currentProjectID = p.projectID;
                        form1.getProjectWindowManager.LoadProject(p);
                    };

                    projectLink.Top = form1.projectListContainer.Controls.Count * projectLink.Height;

                    form1.projectListContainer.Controls.Add(projectLink);
                }
            } catch(NullReferenceException nullref)
            {
                Console.WriteLine("Project list manager, list projects " + nullref.Message);
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Project list manager, list projects " + ex.Message);
            }

            sql.Clean();
        }

        //pick up clicked node and report to project window
        private void TreeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            /*ProjectTreeNode f = (ProjectTreeNode)e.Node;
            form1.getProjectWindowManager.LoadProject(f.projectID);*/
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //create connection form and listen for on click
            connectionForm = new ConnectToDatabase();
            connectionForm.Show();
            connectionForm.btnConnect.Click += btnConnect_Click;
        }

        //pull database values, connect and close connection form
        private void btnConnect_Click(object sender, EventArgs e)
        {
            form1.serverAddress = connectionForm.getServer();
            form1.databaseName = connectionForm.getDatabaseName();
            form1.userName = connectionForm.getUserName();
            form1.password = connectionForm.getPassword();

            ListProjects();
        }

        private void btnAddProject_Click(object sender, EventArgs e)
        {
            //create new project form
            AddProject addProject = new AddProject();
            addProject.Show();

            //get name and description
            String newName = "Not Set";
            String newDescription = "Not Set";
            String newRepository = "Not Set";

            //add projec to database
            addProject.addButton.MouseClick += delegate
            {
                newName = addProject.newName;
                newDescription = addProject.newDescription;
                newRepository = addProject.newRepository;

                String command = "INSERT INTO Projects (projectName, projectDescription, projectRepo) VALUES (\"{0}\", \"{1}\", \"{2}\")";
                command = string.Format(command, newName, newDescription, newRepository);

                //read result
                MySqlDataReader reader = sql.Exec(command);
                while (reader.Read())
                {
                    Console.WriteLine(reader);
                }
                sql.Clean();

                //close add project form
                addProject.Close();

                //refresh projects list
                ListProjects();
            };
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                ArrayList commands = new ArrayList();
                commands.Add("DELETE FROM Projects WHERE projectID = \"{0}\";");
                //commands.Add("DELETE FROM fixed_errors WHERE projectID = \"{0}\";");
                commands.Add("DELETE FROM reported_errors WHERE projectID = \"{0}\";");
                commands.Add("DELETE FROM bugChat WHERE projectID = \"{0}\";");

                //build SQL command
                foreach (string command in commands)
                {
                    string eCommand = string.Format(command, currentProjectID);

                    //read result
                    MySqlDataReader reader = sql.Exec(eCommand);
                    sql.Clean();
                }

                //refresh projects list
                ListProjects();
            }
            catch (NullReferenceException nullError)
            {
                Console.WriteLine("No Project Selected?");
                Console.WriteLine(nullError.Message);
            }
        }
    }
}
