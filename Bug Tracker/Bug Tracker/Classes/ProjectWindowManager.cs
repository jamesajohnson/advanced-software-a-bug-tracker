﻿using Bug_Tracker.Classes;
using Bug_Tracker.UI_Components;
using FastColoredTextBoxNS;
using MetroFramework.Controls;
using Microsoft.Win32;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace Bug_Tracker.Forms.Classes
{
    public class ProjectWindowManager
    {
        private Form1 form1;

        private string projectID;
        private Project project;

        private readonly string noCode = "No Code Supplied";

        private mySQLExec sql;

        public ProjectWindowManager(Form1 form1)
        {
            this.form1 = form1;
            sql = new mySQLExec();

            //row click event
            form1.dtGridErrors.CellClick += DtGridErrors_CellClick;
            form1.dtGridFixed.CellClick += DtGridFixed_CellClick;

            form1.txtCodeBox.TextChanged += fctb_TextChanged;
            form1.txtCodeBox.Language = Language.CSharp;

            form1.btnSubmit.Click += this.btnSubmit_Click;
            form1.btnComment.Click += this.btnComment_Click;

            form1.tbcReport.TabPages.Remove(form1.tbErrors);
            form1.tbcReport.TabPages.Remove(form1.tbReport);
            form1.tbcReport.TabPages.Remove(form1.tbFixed);
            form1.tbcReport.TabPages.Remove(form1.tbBitBucket);

            form1.tbcReport.SelectedIndexChanged += TbcChatCode_SelectedIndexChanged;
        }

        private void DtGridFixed_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            readCode(sender);
            readComments(sender);

            //check cell value
            if (form1.dtGridFixed.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                //was it the check box
                if (form1.dtGridFixed.Rows[e.RowIndex].Cells[e.ColumnIndex].OwningColumn.Name == "Solved")
                {
                    bool checkValue = !bool.Parse(form1.dtGridFixed.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    int intCheck = (checkValue) ? 1 : 0;

                    //update table
                    String command = "UPDATE reported_errors SET solved = \"{0}\" WHERE errorID = {1}";
                    command = string.Format(command, intCheck, form1.dtGridFixed.Rows[e.RowIndex].Cells["ID"].Value);

                    //read result
                    MySqlDataReader reader = sql.Exec(command);
                    while (reader.Read())
                    {
                        Console.WriteLine(reader);
                    }
                    sql.Clean();
                }

                LoadProject(this.project);
            }
        }

        private void DtGridErrors_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            readCode(sender);
            readComments(sender);

            //check cell value
            if (form1.dtGridErrors.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                //was it the check box
                if (form1.dtGridErrors.Rows[e.RowIndex].Cells[e.ColumnIndex].OwningColumn.Name == "Solved")
                {
                    bool checkValue = !bool.Parse(form1.dtGridErrors.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                    int intCheck = (checkValue) ? 1 : 0;

                    //update table
                    String command = "UPDATE reported_errors SET solved = \"{0}\" WHERE errorID = {1}";
                    command = string.Format(command, intCheck, form1.dtGridErrors.Rows[e.RowIndex].Cells["ID"].Value);

                    //read result
                    MySqlDataReader reader = sql.Exec(command);
                    while (reader.Read())
                    {
                        Console.WriteLine(reader);
                    }
                    sql.Clean();
                }

                LoadProject(this.project);

                //MessageBox.Show(form1.dtGridErrors.Rows[e.RowIndex].Cells["ID"].Value + ":" + form1.dtGridErrors.Rows[e.RowIndex].Cells[e.ColumnIndex].OwningColumn.Name + " : " + form1.dtGridErrors.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
            }
        }

        //move comment and code panel between tbErrors and tbFixed
        private void TbcChatCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((sender as TabControl).SelectedTab == form1.tbFixed)
            {
                form1.sptFixed.Panel2.Controls.Add(form1.tbcChatCode);
            }
            else if ((sender as TabControl).SelectedTab == form1.tbErrors)
            {
                form1.sptErrors.Panel2.Controls.Add(form1.tbcChatCode);
            }
        }

        public void LoadProject(Project p)
        {
            this.project = p;
            this.projectID = p.projectID;

            if (form1.tbcReport.TabPages.Contains(form1.tbHome))
            {
                form1.tbcReport.TabPages.Remove(form1.tbHome);
                form1.tbcReport.TabPages.Add(form1.tbErrors);
                form1.tbcReport.TabPages.Add(form1.tbReport);
                form1.tbcReport.TabPages.Add(form1.tbFixed);

                form1.tbcReport.SelectedTab = form1.tbErrors;
            }

            try
            {
                //read error list
                List<Error> errorList = new List<Error>();
                List<Error> fixedList = new List<Error>();

                //connect
                string command = "SELECT * FROM reported_errors WHERE projectID = " + p.projectID;
                MySqlDataReader reader = sql.Exec(command);

                while (reader.Read())
                {
                    Error error = new Error()
                    {
                        ID = reader["errorID"].ToString(),
                        User = reader["userID"].ToString(),
                        Project = reader["projectID"].ToString(),
                        Date = reader["date"].ToString(),
                        Description = reader["errorDescription"].ToString(),
                        Code = reader["code"].ToString(),
                        Solved = Boolean.Parse(reader["solved"].ToString())
                    };

                    if (error.Solved)
                        fixedList.Add(error);
                    else
                        errorList.Add(error);
                }
                reader.Close();

                //add error list to data grid
                form1.dtGridErrors.DataSource = errorList;
                form1.dtGridFixed.DataSource = fixedList;

                sql.Clean();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                MessageBox.Show("error in Project window manager, load project: " + ex.Message);
                MessageBox.Show("Could not connect to the server (MySQLException)");
            }
            catch (NullReferenceException nullref)
            {
                Console.WriteLine("error in Project window manager, load project: " + nullref.Message);
                MessageBox.Show("Could not connect to the server (Null)");
            }

            //populate the bit bucket tab
            buildBitBucketTab(p);
        }

        //populate fields in the BitBucket tab
        private void buildBitBucketTab(Project p)
        {
            //does the project have a linked bitbucket page?
            if (p.projectRepo.Contains("/"))
            {
                if (!form1.tbcReport.TabPages.Contains(form1.tbBitBucket))
                    form1.tbcReport.TabPages.Add(form1.tbBitBucket);

                //pull owner and slug from bit bucket url
                string[] words = p.projectRepo.Split('/');
                p.owner = words[words.Count() - 2];
                p.repo_slug = words[words.Count() - 1];

                //download repo info
                p.RepoInfo(p.owner, p.repo_slug);

                //populate fields
                form1.lbl_bitName.Text = p.name;
                form1.lbl_bitName.Click += delegate { System.Diagnostics.Process.Start(p.links_html); };
                form1.lbl_projectOwner.Text = p.owner_displayname;
                form1.lbl_projectOwner.Click += delegate { System.Diagnostics.Process.Start(p.owner_links_html); };
                form1.lbl_lastUpdated.Text = p.updated_on;
                form1.lbl_projectDescription.Width = form1.pnl_projectInfo.Width;
                form1.lbl_projectDescription.Text = p.description;

                /*
                    TODO Fix so bitbucket is launched
                */
                form1.btn_clnHTTPS.Click += delegate { System.Diagnostics.Process.Start(p.links_clone_https); };
                form1.btn_clnSSH.Click += delegate { System.Diagnostics.Process.Start(p.links_clone_ssh); };

                //picture box
                form1.pb_avatar.Load(p.links_avatar);

                //display issues list
                BitBucketIssues();
            } else
            {
                if(form1.tbcReport.TabPages.Contains(form1.tbBitBucket))
                    form1.tbcReport.TabPages.Remove(form1.tbBitBucket);
            }
        }

        public void BitBucketIssues()
        {
            //load issues
            ArrayList issues = new ArrayList();

            BitBucketREST bbR = new BitBucketREST();
            bbR.RepoIssues(project.owner, project.repo_slug);

            //remove any previous issues
            form1.pnl_RepoIssues.Controls.Clear();

            //add bitbucketissuelistitem to list container in form
            for(int i = 0; i < bbR.issues_size; i++)
            {
                BitBucketIssueListItem issue = new BitBucketIssueListItem(bbR.issues_values_title(i), bbR.issue_created_on(i), bbR.issue_content_html(i), bbR.issue_reported_display_name(i), project.projectRepo + "/issues/" + bbR.issue_id(i));

                Console.WriteLine("count: " + form1.pnl_RepoIssues.Controls.Count);
                Console.WriteLine("height: " + issue.Height);
                Console.WriteLine("* " + form1.pnl_RepoIssues.Controls.Count * issue.Height);
                issue.Top = form1.pnl_RepoIssues.Controls.Count * issue.Height;
                issue.Width = form1.pnl_RepoIssues.Width;

                form1.pnl_RepoIssues.Controls.Add(issue);
                form1.pnl_RepoIssues.Height = form1.pnl_RepoIssues.Controls.Count * issue.Height;
            }

            //set issues scroll bar values
            form1.scr_issues.Maximum = form1.pnl_RepoIssues.Height - form1.pnl_RepoIssuesContainer.Height;
            form1.scr_issues.Minimum = 0;
            form1.scr_issues.Value = 0;
            Console.WriteLine("vScrollBar1 Max: " + form1.pnl_RepoIssues.Height);
            form1.scr_issues.Scroll += delegate
            {
                form1.pnl_RepoIssues.Top = -form1.scr_issues.Value;
            };
        }

        private class Error
        {
            public string ID { get; internal set; }
            public string User { get; internal set; }
            public string Project { get; internal set; }
            public string Date { get; internal set; }
            public string Description { get; internal set; }
            public string Code { get; internal set; }
            public Boolean Solved { get; internal set; }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string error = form1.txtErrorDescription.Text;
            string code = form1.txtCode.Text;

            string codeFileName = noCode;

            //build SQL command

            DateTime myDateTime = DateTime.Now;
            string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd");

            if (code.Length > 0)
                codeFileName = Guid.NewGuid() + ".txt";

            String command = "INSERT INTO reported_errors (userID, errorDescription, projectID, date, code) VALUES (\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\")";
            command = string.Format(command, form1.userID, error, projectID, sqlFormattedDate, codeFileName);

            if (code.Length > 0)
            {
                //create temp code file
                String temp = Path.GetTempPath();
                Console.WriteLine(temp);
                Console.WriteLine(temp + codeFileName);
                File.WriteAllText(temp + codeFileName, code);

                //upload code file
                System.Net.WebClient Client = new System.Net.WebClient();
                Client.Headers.Add("Content-Type", "binary/octet-stream");

                byte[] result = Client.UploadFile("http://jamesliveshere.co/bug_files/upload.php", "POST", temp + codeFileName);
            }

            //read result
            MySqlDataReader reader = sql.Exec(command);
            while (reader.Read())
            {
                Console.WriteLine(reader);
            }
            reader.Close();

            //reload window
            LoadProject(project);

            //select tab 1
            form1.tbcReport.SelectedTab = form1.tbErrors;

            form1.txtErrorDescription.Text = "";
            form1.txtCode.Text = "";
            form1.tbcReport.SelectedIndex = 0;
        }

        private void fctb_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnComment_Click(object sender, EventArgs e)
        {
            MetroGrid dtGrid = null;

            //get selected grid
            if (form1.tbcReport.SelectedTab == form1.tbFixed)
                dtGrid = form1.dtGridFixed;
            else if (form1.tbcReport.SelectedTab == form1.tbErrors)
                dtGrid = form1.dtGridErrors;

            string comment = form1.txtComment.Text;

            String command = "INSERT INTO bugChat (comment, userID, errorID, projectID) VALUES (\"{0}\", \"{1}\", \"{2}\", \"{3}\")";
            command = string.Format(command, comment, form1.userID, dtGrid.CurrentRow.Cells["ID"].Value, this.projectID);

            //read result
            MySqlDataReader reader = sql.Exec(command);
            while (reader.Read())
            {
                Console.WriteLine(reader);
            }
            reader.Close();

            readComments(dtGrid);
            form1.txtComment.Text = "";
        }

        //show chat
        private void readComments(object sender)
        {
            MetroGrid dtGrid = sender as MetroGrid;

            String command = "SELECT bugChat.userID, bugChat.comment, bugChat.projectID, Users.userName FROM bugChat INNER JOIN Users ON Users.userID = bugChat.userID AND bugChat.errorID = {0}";
            //String command = "SELECT bugChat.userID, bugChat.comment, Users.userName FROM bugChat INNER JOIN Users ON Users.userID = bugChat.userID}";
            command = string.Format(command, dtGrid.CurrentRow.Cells["ID"].Value);

            Panel parent = form1.messagesContainer;

            //remove any messages
            form1.messagesContainer.Controls.Clear();
            form1.messagesContainer.Top = 0;

            Panel childContainer = new Panel();
            childContainer.Width = parent.Width;
            bool messages = false;

            //read result and build messages panel
            MySqlDataReader reader = sql.Exec(command);
            while (reader.Read())
            {
                try
                {
                    messages = true;

                    string userID = reader["userID"].ToString();
                    string userName = reader["userName"].ToString();
                    string comment = reader["comment"].ToString();
                    string line = "[{0}] {1}";
                    string text = string.Format(line, userName, comment);

                    //build message panel
                    Panel message = new Panel();
                    Label label = new Label();

                    label.Text = text;
                    label.Width = form1.messagesContainer.Width;

                    //the message is by this user
                    if (userID == form1.userID)
                    {
                        label.TextAlign = ContentAlignment.TopRight;
                    }
                    else
                    {
                        //not by this user
                        label.TextAlign = ContentAlignment.TopLeft;
                    }

                    message.Height = label.Height;
                    message.Width = label.Width;
                    message.Top = message.Height * childContainer.Controls.Count;
                    message.Controls.Add(label);

                    childContainer.Controls.Add(message);
                    childContainer.Height = childContainer.Controls.Count * message.Height;
                }
                catch (NullReferenceException nullref)
                {
                    //no messages to show
                    Console.WriteLine(nullref.Message);
                }
            }
            sql.Clean();

            //create no messages label
            if (!messages)
            {
                Label lblNoMessages = new Label();
                lblNoMessages.Text = "There are no messages";
                lblNoMessages.Width = form1.messagesContainer.Width;
                childContainer.Controls.Add(lblNoMessages);
            }

            //add messages to parent
            parent.Controls.Clear();
            parent.Controls.Add(childContainer);

            //set scroll bar values
            form1.vScrollBar1.Maximum = childContainer.Height - form1.messagesContainer.Height;
            form1.vScrollBar1.Minimum = 0;
            form1.vScrollBar1.Value = 0;
            Console.WriteLine("vScrollBar1 Max: " + childContainer.Height);
            form1.vScrollBar1.Scroll += delegate
            {
                childContainer.Top = -form1.vScrollBar1.Value;
            };
        }

        private void readCode(object sender)
        {
            //get and show code file name if any
            MetroGrid dtGrid = sender as MetroGrid;
            string codeFileName = dtGrid.CurrentRow.Cells["code"].Value.ToString();
            if (codeFileName != "" && codeFileName != noCode)
            {
                try
                {
                    //download file
                    String tempDir = Path.GetTempPath();
                    tempDir += codeFileName;

                    using (var client = new WebClient())
                    {
                        client.DownloadFile("http://jamesliveshere.co/bug_files/files/" + codeFileName, tempDir);
                    }

                    //show download file
                    String code = File.ReadAllText(tempDir);
                    form1.txtCodeBox.Text = code;
                }
                catch (WebException webException)
                {
                    form1.txtCodeBox.Text = webException.Message;
                }
            }
            else
            {
                form1.txtCodeBox.Text = noCode;
            }
        }
    }
}
