﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bug_Tracker.Classes
{
    public class Project: BitBucketREST
    {
        public string projectName { get; internal set; }
        public string projectDescription { get; internal set; }
        public string projectID { get; internal set; }
        public string projectRepo { get; internal set; }
        public string owner { get; internal set; }
        public string repo_slug { get; internal set; }
    }
}
