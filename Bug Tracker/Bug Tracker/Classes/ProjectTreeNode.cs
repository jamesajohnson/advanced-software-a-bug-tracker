﻿using System.Windows.Forms;

namespace Bug_Tracker.Classes
{
    internal class ProjectTreeNode : TreeNode
    {
        public ProjectTreeNode()
        {

        }

        public string projectDescription { get; internal set; }
        public string projectID { get; internal set; }
    }
}