﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Bug_Tracker.Classes
{
    public class BitBucketREST
    {
        //JSON parsers for seperate REST requests
        private JObject repoInfo;
        private JObject repoIssues;

        //get repository information
        public void RepoInfo(string owner, string repo_slug)
        {
            //create url
            string url = "https://bitbucket.org/api/2.0/repositories/{0}/{1}/";
            url = String.Format(url, owner, repo_slug);

            //make request and json object
            var data = makeRequest(url);
            repoInfo = JObject.Parse(data);
        }

        //get repo issues
        public void RepoIssues(string owner, string repo_slug)
        {
            //create url
            string url = "https://bitbucket.org/api/2.0/repositories/{0}/{1}/issues";
            url = String.Format(url, owner, repo_slug);

            //make request and json object
            var data = makeRequest(url);
            repoIssues = JObject.Parse(data);
        }

        //download jsono file
        public string makeRequest(string url) {
            using (WebClient wc = new WebClient())
            {
                string json = "{\"size\":0}";

                try
                {
                    json = wc.DownloadString(url);
                } catch (WebException error)
                {
                    //no issues reported
                }

                return json;
            }
        }

        //return variables pulled from json
        public string scm { get { return repoInfo["scm"].ToString(); } set { scm = value; } }
        public string has_wiki { get { return repoInfo["has_wiki"].ToString(); } set { has_wiki = value; } }
        public string name { get { return repoInfo["name"].ToString(); } set { name = value; } }
        public string fork_policy { get { return repoInfo["fork_policy"].ToString(); } set { fork_policy = value; } }
        public string uuid { get { return repoInfo["uuid"].ToString(); } set { uuid = value; } }
        public string language { get { return repoInfo["language"].ToString(); } set { language = value; } }
        public string created_on { get { return repoInfo["created_on"].ToString(); } set { created_on = value; } }
        public string full_name { get { return repoInfo["full_name"].ToString(); } set { full_name = value; } }
        public string has_issues { get { return repoInfo["has_issues"].ToString(); } set { has_issues = value; } }
        public string updated_on { get { return repoInfo["updated_on"].ToString(); } set { updated_on = value; } }
        public string size { get { return repoInfo["size"].ToString(); } set { size = value; } }
        public string type { get { return repoInfo["type"].ToString(); } set { type = value; } }
        public string is_private { get { return repoInfo["is_private"].ToString(); } set { is_private = value; } }
        public string description { get { return repoInfo["description"].ToString(); } set { description = value; } }

        //project links
        public string links_watchers { get { return repoInfo["links"]["watchers"]["href"].ToString(); } set { links_watchers = value; } }
        public string links_hooks { get { return repoInfo["links"]["hooks"]["href"].ToString(); } set { links_hooks = value; } }
        public string links_clone_https { get { return repoInfo["links"]["clone"][0]["href"].ToString(); } set { links_clone_https = value; } }
        public string links_clone_ssh { get { return repoInfo["links"]["clone"][1]["href"].ToString(); } set { links_clone_ssh = value; } }
        public string links_self { get { return repoInfo["links"]["selk"]["href"].ToString(); } set { links_self = value; } }
        public string links_html { get { return repoInfo["links"]["html"]["href"].ToString(); } set { links_html = value; } }
        public string links_avatar { get { return repoInfo["links"]["avatar"]["href"].ToString(); } set { links_avatar = value; } }
        public string links_commits { get { return repoInfo["links"]["commits"]["href"].ToString(); } set { links_commits = value; } }
        public string links_forks { get { return repoInfo["links"]["forks"]["href"].ToString(); } set { links_forks = value; } }
        public string links_downloads { get { return repoInfo["links"]["downloads"]["href"].ToString(); } set { links_downloads = value; } }
        public string links_pullrequests { get { return repoInfo["links"]["pullrequests"]["href"].ToString(); } set { links_pullrequests = value; } }

        //owner info
        public string owner { get { return repoInfo["owner"].ToString(); } set { owner = value; } }
        public string owner_username { get { return repoInfo["owner"]["username"].ToString(); } set { owner_username = value; } }
        public string owner_displayname { get { return repoInfo["owner"]["display_name"].ToString(); } set { owner_displayname = value; } }
        public string owner_type { get { return repoInfo["owner"]["type"].ToString(); } set { owner_type = value; } }
        public string owner_uuid { get { return repoInfo["owner"]["uuid"].ToString(); } set { owner_uuid = value; } }
        public string owner_links_self { get { return repoInfo["owner"]["links"]["self"]["href"].ToString(); } set { owner_links_self = value; } }
        public string owner_links_html { get { return repoInfo["owner"]["links"]["html"]["href"].ToString(); } set { owner_links_html = value; } }
        public string owner_links_avatar { get { return repoInfo["owner"]["links"]["avatar"]["href"].ToString(); } set { owner_links_avatar = value; } }

        //issue details
        public int issues_size { get { return int.Parse(repoIssues["size"].ToString()); } set { } }
        public string issues_values { get { return repoIssues["values"].ToString(); } set { } }
        public string issues_values_priority(int index) { return repoIssues["values"][index]["priority"].ToString(); }
        public string issues_values_kind(int index) { return repoIssues["values"][index]["kind"].ToString(); }
        public string issues_values_title(int index) { return repoIssues["values"][index]["title"].ToString(); }
        public string issue_created_on(int index) { return repoIssues["values"][index]["created_on"].ToString(); }
        public string issue_id(int index) { return repoIssues["values"][index]["id"].ToString(); }
        public string issue_content_html(int index) { return repoIssues["values"][index]["content"]["html"].ToString(); }
        public string issue_reported_display_name (int index) { return repoIssues["values"][index]["reporter"]["display_name"].ToString(); }
    }
}
