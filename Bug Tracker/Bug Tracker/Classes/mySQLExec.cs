﻿using Bug_Tracker.Forms;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bug_Tracker.Classes
{
    public class mySQLExec
    {
        //connection details
        MySqlConnection conn;
        string myConnectionString;

        //database login cred
        public string serverAddress = "46.32.240.33";
        public string password = "Boulder123!";
        public string userName = "bug-beh-u-021750";
        public string databaseName = "bug-beh-u-021750";

        MySqlDataReader reader;

        public MySqlDataReader Exec(String command) 
        {
            Console.WriteLine("Executing: " + command);
            //set wait cursor
            //form1.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            Cursor.Current = Cursors.WaitCursor;

            //connection info
            myConnectionString = "server=" + serverAddress + ";uid=" + userName + ";" +
                "pwd=" + password + ";database=" + databaseName + ";";

            try
            {
                //connect
                conn = new MySql.Data.MySqlClient.MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();

                //build SQL command
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = command;

                reader = cmd.ExecuteReader();

                return reader;
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.WriteLine("error in mySQLExec: " + ex.Message);
                return null;
                throw;
            }
        }

        public void Clean()
        {
            conn.Close();
            reader.Close();

            Cursor.Current = Cursors.Default;
        }
    }
}
