﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bug_Tracker.Forms
{
    public partial class BitBucketIssueView : MetroForm
    {
        public BitBucketIssueView()
        {
            InitializeComponent();
        }

        public BitBucketIssueView(string title, string issue, string userName, string url)
        {
            InitializeComponent();

            this.Text = title;

            //very hacky solution to format html to rtf, SLOW, NOT GOOD!
            var webBrowser = new WebBrowser();
            webBrowser.CreateControl(); // only if needed
            webBrowser.DocumentText = issue;
            while (webBrowser.DocumentText != issue)
                Application.DoEvents();
            webBrowser.Document.ExecCommand("SelectAll", false, null);
            webBrowser.Document.ExecCommand("Copy", false, null);
            rch_IssueText.Paste();

            lnk_open.Click += delegate
            {
                System.Diagnostics.Process.Start(url);
            };

            lbl_userName.Text = userName;
        }
    }
}
