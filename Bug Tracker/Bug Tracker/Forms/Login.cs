﻿using Bug_Tracker.Classes;
using MetroFramework.Controls;
using MetroFramework.Forms;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bug_Tracker.Forms
{
    public partial class Login : MetroForm
    {

        private Crypto crypto = new Crypto();

        public Login()
        {
            InitializeComponent();

            txtUserName.Click += ClearTextBoxText;
            txtPassword.Click += ClearTextBoxText;

            btnLogin.Click += BtnLogin_Click;
            btnRegister.Click += BtnRegister_Click;

            //message panel
            this.pnlMessage.Visible = false;
            btnOk.Click += delegate
            {
                this.pnlMessage.Visible = false;
            };

        }

        //register new user
        private void BtnRegister_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text == "User Name" || txtPassword.Text == "Password")
            {
                showMessage("User name or Password missing.");
            }
            else
            {
                string userName = txtUserName.Text;
                string userPassword = crypto.EncryptStringAES(txtPassword.Text, "bugcrypt");

                string command = "INSERT INTO Users (userName, userPassword) VALUES (\"{0}\", \"{1}\")";
                command = string.Format(command, userName, userPassword);

                MySqlDataReader reader = new mySQLExec().Exec(command);
                try
                {
                    if (reader.FieldCount > 0)
                    {
                    }
                    showMessage("Succesfully Registered.");
                    btnOk.Click += delegate
                    {
                        BtnLogin_Click(null, null);
                    };
                }
                catch (NullReferenceException nullref)
                {
                    showMessage("User name unavailable.");
                }
            }
        }

        //check login cred
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if(txtUserName.Text == "User Name" || txtPassword.Text == "Password")
            {
                showMessage("User name or Password missing.");
            } else
            {
                MySqlDataReader reader = new mySQLExec().Exec("SELECT * FROM Users");

                bool login = false;
                
                while (reader.Read())
                {
                    string dbUserName = reader["userName"].ToString();
                    string dbUserPassword = reader["userPassword"].ToString();
                    dbUserPassword = crypto.DecryptStringAES(dbUserPassword, "bugcrypt");

                    string userName = txtUserName.Text;
                    string userPassword = txtPassword.Text;

                    if (dbUserName == userName && dbUserPassword == userPassword)
                    {
                        login = true;
                        showForm1(reader["userID"].ToString());
                        break;
                    }
                }

                if(!login)
                {
                    showMessage("Incorrect login details.");
                }
            }
        }

        private void showForm1(string userID)
        {
            //show the main form & join close actions
            Form1 form1 = new Form1();
            form1.userID = userID;
            form1.ShowInTaskbar = true;
            form1.FormClosed += delegate
            {
                this.Close();
            };

            form1.Show();
            this.Hide();
        }

        private void ClearTextBoxText(object sender, EventArgs e)
        {
            txtPassword.PasswordChar = '*';
            (sender as MetroTextBox).Text = "";
        }

        private void showMessage(string message) {
            this.pnlMessage.Visible = true;
            lblMessage.Text = message;
        }

    }
}
