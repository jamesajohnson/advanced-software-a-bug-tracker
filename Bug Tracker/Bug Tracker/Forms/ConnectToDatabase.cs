﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bug_Tracker.Forms
{
    public partial class ConnectToDatabase : Form
    {
        public ConnectToDatabase()
        {
            InitializeComponent();
        }

        public string getServer()
        {
            return this.txtServer.Text;
        }
        
        public string getDatabaseName()
        {
            return this.txtDatabase.Text;
        }

        public string getUserName()
        {
            return this.txtUser.Text;
        }

        public string getPassword()
        {
            return this.txtPassword.Text;
        }


    }
}
