﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bug_Tracker.Forms
{
    public partial class AddProject : Form
    {
        public AddProject()
        {
            InitializeComponent();
        }

        //return the add button from the form
        public Button addButton { get { return this.btnAdd; } set { this.btnAdd = value; } }
        public String newName { get { return this.txtName.Text; } set { this.newName = value; } }
        public String newDescription { get { return this.txtDesc.Text; } set { this.newDescription = value; } }
        public String newRepository { get { return this.txtRepository.Text; } set { this.newRepository = value; } }
    }
}
