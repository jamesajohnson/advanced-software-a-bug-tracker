﻿using Bug_Tracker.Classes;
using MetroFramework.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bug_Tracker.UI_Components
{
    class myMetroLink : MetroLink
    {

        public myMetroLink()
        {

        }

        public myMetroLink(Project project)
        {
            FontSize = MetroFramework.MetroLinkSize.Medium;
            FontWeight = MetroFramework.MetroLinkWeight.Regular;
            Location = new System.Drawing.Point(3, 3);
            Name = "metroLink1";
            Size = new System.Drawing.Size(169, 37);
            TabIndex = 2;
            Text = project.projectName;
            TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            UseSelectable = true;
        }
    }
}
