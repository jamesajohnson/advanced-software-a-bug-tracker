﻿using Bug_Tracker.Forms;
using MetroFramework.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bug_Tracker.UI_Components
{
    class BitBucketIssueListItem: MetroPanel
    {
        private string url;

        //create a panel containg a title and date for each issue
        public BitBucketIssueListItem()
        {
            MetroLabel titleLbl = new MetroLabel();
            titleLbl.Text = "issue title";
            titleLbl.Left = 0;
            titleLbl.Top = 0;
            titleLbl.Width = this.Width;

            MetroLabel dataLbl = new MetroLabel();
            dataLbl.Text = "01/01/2001";
            dataLbl.Left = this.Width - dataLbl.Width;
            dataLbl.Top = titleLbl.Bottom;

            this.Height = dataLbl.Height + titleLbl.Height;

            this.Controls.Add(titleLbl);
            this.Controls.Add(dataLbl);
        }

        //create a panel containg a title and date for each issue
        public BitBucketIssueListItem(string title, string date, string issue, string reporter, string url)
        {
            this.url = url;

            MetroLabel titleLbl = new MetroLabel();
            titleLbl.Text = title;
            titleLbl.Left = 0;
            titleLbl.Top = 0;
            titleLbl.Width = this.Width;

            MetroLabel dataLbl = new MetroLabel();
            dataLbl.Text = date;
            dataLbl.Left = 0;
            dataLbl.Top = titleLbl.Bottom;

            this.Height = dataLbl.Height + titleLbl.Height;

            this.Controls.Add(titleLbl);
            this.Controls.Add(dataLbl);

            titleLbl.Click += delegate
            {
                BitBucketIssueView bbIView = new BitBucketIssueView(title, issue, reporter, url);
                bbIView.Show();
            };
        }
    }
}
